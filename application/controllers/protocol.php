<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Protocol extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('asfb/dbconfig_m', 'dbconfig');
		$this->dbconfig->load_config('ASFB');
		$this->load->helper(array('html', 'url'));
	}

	function index()
	{
		$ctrpath = 'asfb/index.php/asfb';

		$protocol = array('user', 'achievement', 'mission', 'hangar', 'record', 'battle', 'shop');

		$sub['user'] = array('start', 'save_name', 'save_passive', 'reset_passive', 'info');
		$sub['achievement'] = array();
		$sub['mission'] = array();
		$sub['hangar'] = array('save_part', 'save_plane', 'repair_plane');
		$sub['record'] = array();
		$sub['battle'] = array('start_battle', 'save_battle');
		$sub['shop'] = array('buy_part', 'buy_medal');
		
		$param['start'] = array('device', 'name');
		$param['save_name'] = array('uid', 'name');
		$param['save_passive'] = array('uid', 'p1', 'p2', 'p3', 'p4');
		$param['reset_passive'] = array('uid');
		$param['info'] = array('uid');
		
		$param['save_part'] = array('uid', 'part1', 'part2', 'part3', 'part4', 'part5');
		$param['save_plane'] = array('uid', 'plane');
		$param['repair_plane'] = array('uid', 'plane', 'repair');
		
		$param['start_battle'] = array('uid');
		$param['save_battle'] = array('uid', 'result', 'medal');
		
		$param['buy_part'] = array('uid', 'part');
		$param['buy_medal'] = array('uid', 'medal');
		
		if ($this->input->get('mode')) $this->load->view('protocol_result_v', array('mode' => $this->input->get('mode'),
				'param' => $param[$this->input->get('mode')], 'ctrpath' => $ctrpath));
		else $this->load->view('protocol_index_v', array('protocol' => $protocol, 'sub' => $sub));
	}

	function test($index = 0)
	{
//		echo phpinfo();
	}

	function test1()
	{
		$rank = 38100;
		
		$base = array(0 => 500, 1 => 2500, 2 => 5000, 3 => 10000, 4 => 20000, 5 =>50000, 6 => 0);
		$return['league'] = 1;
        $return['level'] = 1;
		$sum = $base[0];
		$prev = 0;
		for ($i = 1; $i < count($base); $i++)
		{
			if (floor($rank / $sum) < 1)
			{
				$sum1 = $base[$i - 1] / 5;
				$rank -= $prev;
				for ($j = 0; $j < 5; $j++)
				{
        			if (floor($rank / ($sum1)) < 1) break;
        			$return['level']++;
        			$sum1 += $sum1;
				}
				break;
			}
			$return['league']++;
			$sum += $base[$i];
			$prev += $base[$i - 1];
		}
		
		echo $return['league'];
	}
	
	function cache_clear($target = '', $key = '')
	{
		$this->load->driver('cache');
		
		if ($target == 'apc')
		{
			$this->cache->apc->clean();			
		}
		else if ($target == 'memcached')
		{
			$this->cache->memcached->clean();
		}
		else if ($target == 'redis')
		{
			redis()->del($key);
		}
	}
}

/* End of file */