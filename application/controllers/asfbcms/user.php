<?php
class User extends Base_class_cms
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->header_view();

		$this->load->view('asfbcms/user_v');

		$this->footer_view();
	}
}

/* End of file */