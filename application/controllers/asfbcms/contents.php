<?php
class Contents extends Base_class_cms
{
	function __construction()
	{
		parent::__construct();
	}

	function index()
	{
		$this->header_view();

		$this->load->view('asfbcms/contents_v');

		$this->footer_view();
	}
}

/* End of file */