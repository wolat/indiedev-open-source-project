<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Base_class_cms extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();

		set_exception_handler(array($this, 'exception_handler'));

		$this->load_dbtable();
		
		$this->check_auth();
		
		$this->load->helper(array('form', 'html', 'url'));
	}
	
	function exception_handler(Exception $e)
	{
		log_message('error', $e->getMessage());
		exit('Error!');
	}

	function load_dbtable()
	{
		$this->load->driver('cache');
		$this->load->model('asfb/dbconfig_m', 'dbconfig');
		$this->dbconfig->load_config('ASFB');
		$this->load->model('asfb/dbtable_m', 'dbtable');
	}
	
	function header_view()
	{
		$this->load->view('asfbcms/header_v');
	}
	
	function footer_view()
	{
		$this->load->view('asfbcms/footer_v');
	}

	function check_from($iplist)
	{
		if (empty($iplist))
		{
			return TRUE;
		}
		
		if (in_array($_SERVER['REMOTE_ADDR'], $iplist))
		{
			return TRUE;
		}
		
		return FALSE;
	}
	
	function check_auth()
	{
		if (!$this->check_from($this->dbconfig->fileconfig['asfbcms_ip_allowed'])) 
		{
			$this->load->view('asfbcms/auth_fail_v');
			exit;
		}
		
		if (!$this->auth_digest()) 
		{
			$this->load->view('asfbcms/auth_fail_v');
			exit;
		}
	}

	function auth_digest()
	{
		$users = array($this->dbconfig->fileconfig['asfbcms_user'] => $this->dbconfig->fileconfig['asfbcms_pass']);
		$realm = array('realm' => $this->dbconfig->fileconfig['asfbcms_realm']);

		if (empty($_SERVER['PHP_AUTH_DIGEST'])) 
		{
			$this->load->view('asfbcms/auth_challenge_v', $realm);
		}

		if (!($data = $this->http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
    		!isset($users[$data['username']])) 
		{
			return FALSE;
		}

		$A1 = md5($data['username'] . ':' . $realm['realm'] . ':' . $users[$data['username']]);
		$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
		$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

		if ($data['response'] != $valid_response) 
		{
			return FALSE;
		}

		return TRUE;
	}

	function http_digest_parse($txt)
	{
   		$needed_parts = array('nonce' => 1, 'nc' => 1, 'cnonce' => 1, 'qop' => 1, 'username' => 1, 'uri' => 1, 'response' => 1);
   		$data = array();
   		$keys = implode('|', array_keys($needed_parts));

   		preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

   		foreach ($matches as $m) 
   		{
    		$data[$m[1]] = $m[3] ? $m[3] : $m[4];
    		unset($needed_parts[$m[1]]);
   		}

   		return $needed_parts ? FALSE : $data;
	}

}

/* End of file */
