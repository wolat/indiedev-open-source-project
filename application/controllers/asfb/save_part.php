<?php
class Save_part extends Base_class
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('asfb/user_part_m', 'upm');
    }

    function index()
    {
    	if (FALSE === ($ret = $this->post_validation(array('uid', 'part1', 'part2', 'part3', 'part4', 'part5')))) return;
    	$this->load->view('asfb/json_v', array('return' => $this->upm->save($ret)));
    }
}

/* End of file */