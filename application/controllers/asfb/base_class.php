<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Base_class extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		$this->init();
	}
	
	function init()
	{
		set_exception_handler(array($this, 'exception_handler'));
		
		$this->load->driver('cache');
		
		$this->load_config();
		
		$this->load_dbtable();
	}

	function exception_handler(Exception $e)
	{
		$this->error_view('', $e->getMessage());
		exit;
	}

	function error_view($msg = '', $log = '', $pret = FALSE)
	{
		if ($log !== '') log_message('error', $log);

		$this->load->view('asfb/json_v', array("return" => array("result" => FALSE, "msg" => $msg)), $pret);
	}

	function post_validation($param = array())
	{
		$in = json_decode(file_get_contents('php://input'), true);
		$ret = json_decode(base64_decode($in['param']), true);
		foreach ($param as $p)
    	{
    		if (!isset($ret[$p]))
    	    {
				$this->error_view('Parameter needed - ' . $p);
				return FALSE;
	    	}
				
			$check = $ret[$p];
    		if ($check === '' or $check === NULL)
    		{
				$this->error_view('Invalid parameter - ' . $p);
				return FALSE;
			}
		}
		
		return $ret;
	}

	function load_dbtable()
	{
		$this->load->model('asfb/dbtable_m', 'dbtable');
	}
	
	function load_config()
	{
		$this->load->model('asfb/dbconfig_m', 'dbconfig');
		$this->dbconfig->load_config('ASFB');
	}
}

/* End of file */