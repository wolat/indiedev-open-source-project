<?php
class Start extends Base_class
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('asfb/start_m', 'sm');
    }

    function index()
    {
    	if (FALSE === ($ret = $this->post_validation(array('device')))) return;
	  	$this->load->view('asfb/json_v', array('return' => $this->sm->start($ret)));
    }
}

/* End of file */