<?php
class Save_name extends Base_class
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('asfb/user_m', 'um');
    }

    function index()
    {
    	if (FALSE === ($ret = $this->post_validation(array('uid', 'name')))) return;
    	$this->load->view('asfb/json_v', array('return' => $this->um->save_name($ret)));
    }
}

/* End of file */