<?php
class Save_plane extends Base_class
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('asfb/user_plane_m', 'upm');
    }

    function index()
    {
    	if (FALSE === ($ret = $this->post_validation(array('uid', 'plane')))) return;
    	$this->load->view('asfb/json_v', array('return' => $this->upm->save($ret)));
    }
}

/* End of file */