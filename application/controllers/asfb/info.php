<?php
class Info extends Base_class
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('asfb/user_m', 'um');
    }

    function index()
    {
    	if (FALSE === ($ret = $this->post_validation(array('uid')))) return;
    	$this->load->view('asfb/json_v', array('return' => $this->um->info($ret)));
    }
}

/* End of file */