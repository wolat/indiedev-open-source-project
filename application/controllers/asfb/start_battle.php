<?php
class Start_battle extends Base_class
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('asfb/battle_m', 'bm');
    }

    function index()
    {
    	if (FALSE === ($ret = $this->post_validation(array('uid')))) return;
    	$this->load->view('asfb/json_v', array('return' => $this->bm->start($ret)));
    }
}

/* End of file */