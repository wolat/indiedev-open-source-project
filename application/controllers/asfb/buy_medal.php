<?php
class Buy_medal extends Base_class
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('asfb/shop_m', 'sm');
    }

    function index()
    {
    	if (FALSE === ($ret = $this->post_validation(array('uid', 'medal')))) return;
    	$this->load->view('asfb/json_v', array('return' => $this->sm->buy_medal($ret)));
    }
}

/* End of file */