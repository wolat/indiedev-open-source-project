<?php
class Reset_passive extends Base_class
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('asfb/user_passive_m', 'upm');
    }

    function index()
    {
    	if (FALSE === ($ret = $this->post_validation(array('uid')))) return;
    	$this->load->view('asfb/json_v', array('return' => $this->upm->reset($ret)));
    }
}

/* End of file */