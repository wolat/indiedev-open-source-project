<?php
class Save_passive extends Base_class
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('asfb/user_passive_m', 'upm');
    }

    function index()
    {
    	if (FALSE === ($ret = $this->post_validation(array('uid', 'p1', 'p2', 'p3', 'p4')))) return;
    	$this->load->view('asfb/json_v', array('return' => $this->upm->save($ret)));
    }
}

/* End of file */