<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Start_m extends CI_Model
{
    function __construct()
  	{
        parent::__construct();

   		$this->load->database();
  	}

    function start($param = array())
    {
        $return['result'] = FALSE;
        $return['uid'] = 0;
        
   	    $this->db->trans_start();
    	 
    	$sql = "select d_u_id from " . $this->dbtable->get('t_device') . " where d_id = ?;";
    	$query = $this->db->query($sql, array($param['device']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return['uid'] = $query->row()->d_u_id;
    	    $sql = "update " . $this->dbtable->get('t_user', $return['uid']) . " set u_lastdate=now() where u_id=?;";
    		if (!$this->db->query($sql, array($return['uid'])))
    		{
    			throw new Exception ('start_m - start - ' . $this->db->last_query());
    		}
    		$query->free_result();
    	}
    	else
    	{
    		$sql = "insert into " . $this->dbtable->get('t_user') . "(u_regdate, u_lastdate) values(now(), now());";
    		if (!$this->db->query($sql))
    		{
    			throw new Exception ('start_m - start - ' . $this->db->last_query());
    		}
    		
    		$sql = "select last_insert_id() as id;";
    		$query = $this->db->query($sql);
    		if (!empty($query) && $query->num_rows() > 0)
    		{
    			$return['uid'] = $query->row()->id;
    		    $sql = "insert into " . $this->dbtable->get('t_device') . "(d_id, d_u_id, d_name) values(?, ?, ?) on duplicate key update d_u_id=values(d_u_id), d_name=values(d_name);";
    			if (!$this->db->query($sql, array($param['device'], $return['uid'], base64_encode($param['name']))))
    			{
    				throw new Exception ('start_m - start - ' . $this->db->last_query());
    			}
	    		$query->free_result();
    		}
    	}
    	
    	if (!$this->db->trans_complete())
    	{
    		throw new Exception('start_m - start - transaction');
    	}

    	if ($return['uid'] != 0) $return['result'] = TRUE;
    	else return $return;

        $sql = "select u_name, u_cash, u_medal, u_rank, u_exp from " . $this->dbtable->get('t_user', $return['uid']) . " where u_id = ?;";
    	$query = $this->db->query($sql, array($return['uid']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return['user'] = $query->row_array();
	    	$query->free_result();
    	}
    	
        $this->load->model('asfb/user_achievement_m', 'user_achievementm');
   		$return['achievement'] = $this->user_achievementm->get($return['uid']);
   		 
   		$this->load->model('asfb/user_mission_m', 'user_missionm');
   		$return['mission'] = $this->user_missionm->get($return['uid']);
   		 
        $this->load->model('asfb/user_part_m', 'user_partm');
   		$return['part'] = $this->user_partm->get($return['uid']);
   		 
   		$this->load->model('asfb/user_passive_m', 'user_passivem');
   		$return['passive'] = $this->user_passivem->get($return['uid']);
   		 
   		$this->load->model('asfb/user_plane_m', 'user_planem');
   		$return['plane'] = $this->user_planem->get($return['uid']);
   		 
   		$this->load->model('asfb/user_record_m', 'user_recordm');
   		$return['record'] = $this->user_recordm->get($return['uid']);
   		 
   		$this->load->model('asfb/user_skin_m', 'user_skinm');
   		$return['skin'] = $this->user_skinm->get($return['uid']);
   		 
   		$return['revision'] = $this->dbconfig->fileconfig['REVISION'];
   		 
        return $return;
    }
}

/* End of file */