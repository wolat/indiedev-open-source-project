<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_m extends CI_Model
{
    function __construct()
 	{
        parent::__construct();

   		$this->load->database();
   	}
   	
    function save_name($param = array())
    {
        $return['result'] = FALSE;
        
        $this->load->model('asfb/wordfilter_m', 'wordfilterm');
        $return['name'] = base64_encode($this->wordfilterm->word_censor(mb_substr($param['name'], 0, 20)));

   	    $sql = "update " . $this->dbtable->get('t_user', $param['uid']) . " set u_name=? where u_id=?;";
   		if (!$this->db->query($sql, array($return['name'], $param['uid'])))
   		{
   			throw new Exception ('user_m - save_name - ' . $this->db->last_query());
   		}
    	 
        $return['result'] = TRUE;
   		
        return $return;
    }
    
    function info($param = array())
    {
    	$return['result'] = FALSE;
    
    	$this->load->model('asfb/user_part_m', 'user_partm');
    	$return['part'] = $this->user_partm->get_use($param['uid']);
    	
    	$this->load->model('asfb/user_passive_m', 'user_passivem');
    	$return['passive'] = $this->user_passivem->get($param['uid']);
    	 
   		$this->load->model('asfb/user_skin_m', 'user_skinm');
   		$return['skin'] = $this->user_skinm->get($param['uid']);
    	
   		$return['result'] = TRUE;
    	 
    	return $return;
    }
    
    function save_battle($param = array())
    {
    	$return['medal'] = 0;
    	$return['rank'] = 0;
    	$return['exp'] = 0;
    	$sql = "select u_medal, u_rank, u_exp from " . $this->dbtable->get('t_user', $param['uid']) . " where u_id=? for update;";
    	$query = $this->db->query($sql, array($param['uid']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return['medal'] = $query->row()->u_medal;
    		$return['rank'] = $query->row()->u_rank;
    		$return['exp'] = $query->row()->u_exp;
    		$query->free_result();
    	}
    	
    	$return['medal'] += $param['medal'];
    	$return['exp'] += $param['medal'];
    	$sum1 = 0;
    	$sum2 = 0;
    	for ($i = 1; $i <= $return['rank']; $i++)
    	{
    	$sum1 += pow($i, 2) * 500;
    	}
    	$sum2 = $sum1 + pow($return['rank'] + 1, 2) * 500;
    	if ($sum2 < $return['exp'] - $sum1) $return['rank']++;
    	
    	$sql = "update " . $this->dbtable->get('t_user', $param['uid']) . " set u_medal=?, u_rank=?, u_exp=? where u_id=?;";
       	if (!$this->db->query($sql, array($return['medal'], $return['rank'], $return['exp'], $param['uid'])))
       	{
   	       	throw new Exception ('user_m - save_battle - ' . $this->db->last_query());
       	}
       	
       	return $return;
    }
}

/* End of file */