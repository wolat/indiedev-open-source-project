<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_record_m extends CI_Model
{
    function __construct()
  	{
        parent::__construct();

   		$this->load->database();
  	}

    function get($uid = '')
    {
        $return = '';

    	$sql = "select ur_score from " . $this->dbtable->get('t_user_record', $uid) . " where ur_u_id = ?;";
    	$query = $this->db->query($sql, array($uid));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return = $query->row_array();
    		$query->free_result();
    	}
    	else $return['ur_score'] = 0;
        
        $return += $this->get_league($uid);
    	
        return $return;
    }
    
    function get_league($uid = '')
    {
        $return['season'] = $this->dbconfig->fileconfig['SEASON'];
    	$key = "ranking_" . $return['season'];
        $rank = redis()->zrevrank($key, $uid);
        $base = array(0 => 500, 1 => 2500, 2 => 5000, 3 => 10000, 4 => 20000, 5 =>50000, 6 => 0);
		$return['league'] = 1;
        $return['level'] = 1;
		$sum = $base[0];
		$prev = 0;
		for ($i = 1; $i < count($base); $i++)
		{
			if (floor($rank / $sum) < 1)
			{
				$sum1 = $base[$i - 1] / 5;
				$rank -= $prev;
				for ($j = 0; $j < 5; $j++)
				{
        			if (floor($rank / ($sum1)) < 1) break;
        			$return['level']++;
        			$sum1 += $sum1;
				}
				break;
			}
			$return['league']++;
			$sum += $base[$i];
			$prev += $base[$i - 1];
		}
		
		return $return;
    }
    
    function save_battle($param = array())
    {
    	$score = 0;
    	$win = 0;
    	$lose = 0;
    	$draw = 0;
    	if ($param['result'] == 0)
    	{
    		$win++;
    		$score += 10 + $param['medal'];
    	}
    	else if ($param['result'] == 1)
    	{
    		$lose++;
    		$score += 1 + $param['medal'];
    	}
    	else if ($param['result'] == 2)
    	{
    		$draw++;
    		$score += 5 + $param['medal'];
    	}
    	
    	$redis = redis();
    	$key = "ranking_" . $this->dbconfig->fileconfig['SEASON'];
    	$redis->zadd($key, $score, $param['uid']);
    	$return['score'] = $redis->zscore($key, $param['uid']);
    	
    	$sql = "insert into " . $this->dbtable->get('t_user_record', $param['uid']) . "(ur_u_id, ur_score, ur_win, ur_lose, ur_draw, ur_dis, ur_time) values(?, ?, ?, ?, ?, 0, now())
    			on duplicate key update ur_score=ur_score+values(ur_score),ur_win=ur_win+values(ur_win),ur_lose=ur_lose+values(ur_lose), ur_draw=ur_draw+values(ur_draw), ur_time=values(ur_time);";
    	if (!$this->db->query($sql, array($param['uid'], $score, $win, $lose, $draw)))
    	{
    		throw new Exception ('user_record_m - save_battle - ' . $this->db->last_query());
    	}
    	
		$return += $this->get_league($param['uid']);
		
        return $return;
    }
}

/* End of file */