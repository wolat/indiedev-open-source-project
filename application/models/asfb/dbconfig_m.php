<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dbconfig_m extends CI_Model
{
	var $fileconfig = NULL;
	
	function __constuct()
	{
		parent::__construct();
	}
	
	function load_config($pmodule = NULL)
	{
		$this->config->load('asfb_config', TRUE);
		$cfg = $this->config->item($pmodule, 'asfb_config');
		$cfg['config'] = $pmodule;
		$this->fileconfig = $cfg;
	}		
	
}

/* End of file */