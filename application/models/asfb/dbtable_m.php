<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dbtable_m extends CI_Model 
{
	function __construct()
  	{
        parent::__construct();
  	}
  	
  	function get($ptable = '', $pkey = 0)
  	{
  		switch ($ptable)
		{
			case 't_user' :
				return $ptable;
//				return $ptable . '_' . (1 + $pkey - 1) % 100;
			case 't_user_record' :
				return $ptable . '_' . $this->dbconfig->fileconfig['SEASON'];
			default :
				return $ptable;
  		}
  	}

	function make_userid_group($ptable = '', $pids = NULL)
	{
		$ta = array();
		foreach ($pids as $id)
		{
			$tn = $this->get($ptable, $id);
			if (array_key_exists($tn, $ta))
			{
				$ta[$tn] = array_merge($ta[$tn], array($id));
			}
			else
			{
				$ta[$tn] = array($id);
			}
		}
		
		return $ta;
	}
}

/* End of file */