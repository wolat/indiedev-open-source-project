<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_part_m extends CI_Model
{
    function __construct()
  	{
        parent::__construct();

   		$this->load->database();
  	}

    function get($uid = '')
    {
        $return = '';

    	$sql = "select up_item, up_quantity, up_use, up_part from " . $this->dbtable->get('t_user_part', $uid) . " where up_u_id = ?;";
    	$query = $this->db->query($sql, array($uid));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return = $query->result_array();
    		$query->free_result();
    	}
        
        return $return;
    }

    function get_use($uid = '')
    {
        $return = '';

    	$sql = "select up_item, up_part from " . $this->dbtable->get('t_user_part', $uid) . " where up_u_id = ? and up_use=1;";
    	$query = $this->db->query($sql, array($uid));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return = $query->result_array();
    		$query->free_result();
    	}
        
        return $return;
    }

    function save($param = array())
    {
        $return['result'] = FALSE;

        $this->db->trans_start();
    	 
        $rank = 0;
        $uid = 0;
        $sql = "select u_id, u_rank from " . $this->dbtable->get('t_user', $param['uid']) . " where u_id=? for update;";
    	$query = $this->db->query($sql, array($param['uid']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$rank = $query->row()->u_rank;
    		$uid = $query->row()->u_id;
    		$query->free_result();
    	}
    	
    	if ($uid != $param['uid']) return $return;
    	 
    	$sql = "select ip_index, ip_rank from " . $this->dbtable->get('t_item_part') . " where ip_index in (?, ?, ?, ?, ?) for update;";
    	$query = $this->db->query($sql, array($param['part1'], $param['part2'], $param['part3'], $param['part4'], $param['part5']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		foreach ($query->result() as $row)
    		{
    			if ($row->ip_index > 0 && $rank < $row->ip_rank)
    			{
    				return $return;
    			}
    		}
    		$query->free_result();
    	}
    	
    	$sql = "update " . $this->dbtable->get('t_user_part', $param['uid']) . " set up_use=0 where up_u_id=?;";
   		if (!$this->db->query($sql, array($param['uid'])))
   		{
   			throw new Exception ('user_part_m - save - ' . $this->db->last_query());
   		}
    	 
        $sql = "update " . $this->dbtable->get('t_user_part', $param['uid']) . " set up_use=1 where up_u_id=? and up_item in (?, ?, ?, ?, ?);";
   		if (!$this->db->query($sql, array($param['uid'], $param['part1'], $param['part2'], $param['part3'], $param['part4'], $param['part5'])))
   		{
   			throw new Exception ('user_part_m - save - ' . $this->db->last_query());
   		}
    	 
   		if (!$this->db->trans_complete())
    	{
    		throw new Exception('user_part_m - save - transaction');
    	}
   		
    	$return['result'] = TRUE;

    	return $return;
    }
    
    function spend($param = array())
    {
        $sql = "select up_item, up_quantity from " . $this->dbtable->get('t_user_part', $param['uid']) . " where up_u_id=? and up_use=1 for update;";
   	    	$query = $this->db->query($sql, array($param['uid']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		foreach ($query->result() as $row)
    		{
    			if ($row->up_quantity > 1)
    			{
    				$sql = "update " . $this->dbtable->get('t_user_part', $param['uid']) . " set u_quantity=u_quantity-1 where up_u_id=? and up_item=?;";
       				if (!$this->db->query($sql, array($param['uid'], $row->up_item)))
   					{
   						throw new Exception ('user_part_m - spend - ' . $this->db->last_query());
   					}
    			}
    			else
    			{
    				$sql = "delete from " . $this->dbtable->get('t_user_part', $param['uid']) . " where up_u_id=? and up_item=?;";
       				if (!$this->db->query($sql, array($param['uid'], $row->up_item)))
   					{
   						throw new Exception ('user_part_m - spend - ' . $this->db->last_query());
   					}
    			}
    		}
    		$query->free_result();
    	}
    }
}

/* End of file */