<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_passive_m extends CI_Model
{
    function __construct()
  	{
        parent::__construct();

   		$this->load->database();
  	}

    function get($uid = '')
    {
        $return = '';

    	$sql = "select up_p1, up_p2, up_p3, up_p4 from " . $this->dbtable->get('t_user_passive', $uid) . " where up_u_id = ?;";
    	$query = $this->db->query($sql, array($uid));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return = $query->row_array();
    		$query->free_result();
    	}
        
        return $return;
    }
   	
    function save($param = array())
    {
        $return['result'] = FALSE;

        $this->db->trans_start();
    	 
        $sql = "select u_rank from " . $this->dbtable->get('t_user', $param['uid']) . " where u_id=? for update;";
    	$query = $this->db->query($sql, array($param['uid']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		if ($param['p1'] + $param['p2'] + $param['p3'] + $param['p4'] > $query->row()->u_rank + 10) return $return;
    		$query->free_result();
    	}
   	    
    	$sql = "insert into " . $this->dbtable->get('t_user_passive', $param['uid']) . "(up_u_id, up_p1, up_p2, up_p3, up_p4) values(?, ?, ?, ?, ?) on duplicate key update up_p1=values(up_p1), 
    			up_p2=values(up_p2), up_p3=values(up_p3), up_p4=values(up_p4);";
   		if (!$this->db->query($sql, array($param['uid'], $param['p1'], $param['p2'], $param['p3'], $param['p4'])))
   		{
   			throw new Exception ('user_passive_m - save - ' . $this->db->last_query());
   		}
    	 
        if (!$this->db->trans_complete())
    	{
    		throw new Exception('user_passive_m - save - transaction');
    	}
   		
    	$return['result'] = TRUE;

    	return $return;
    }
   	
    function reset($param = array())
    {
        $return['result'] = FALSE;
        
    	$sql = "insert into " . $this->dbtable->get('t_user_passive', $param['uid']) . "(up_u_id, up_p1, up_p2, up_p3, up_p4) values(?, 0, 0, 0, 0) on duplicate key update up_p1=values(up_p1), 
    			up_p2=values(up_p2), up_p3=values(up_p3), up_p4=values(up_p4);";
   		if (!$this->db->query($sql, array($param['uid'])))
   		{
   			throw new Exception ('user_passive_m - reset - ' . $this->db->last_query());
   		}
    	 
    	$return['result'] = TRUE;
   		
        return $return;
    }
}

/* End of file */