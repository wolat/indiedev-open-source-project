<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_plane_m extends CI_Model
{
    function __construct()
  	{
        parent::__construct();

   		$this->load->database();
  	}

    function get($uid = '')
    {
        $return = '';

    	$this->db->trans_start();
    	 
        $sql = "select * from " . $this->dbtable->get('t_user_plane', $uid) . " where up_u_id = ? for update;";
    	$query = $this->db->query($sql, array($uid));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return = $query->result_array();
    		$query->free_result();
    	}
    	else
    	{
    		$sql = "insert into " . $this->dbtable->get('t_user_plane', $uid) . "(up_u_id, up_plane, up_use, up_time) values(?, 1, 1, now()) 
        		on duplicate key update up_use=values(up_use), up_time=values(up_time);";
   			if (!$this->db->query($sql, array($uid)))
   			{
   				throw new Exception ('user_plane_m - get - ' . $this->db->last_query());
   			}
    	}

    	if (!$this->db->trans_complete())
    	{
    		throw new Exception('user_plane_m - get - transaction');
    	}
    	
        return $return;
    }

    function save($param = array())
    {
        $return['result'] = FALSE;

        $this->db->trans_start();
    	 
        $rank = 0;
        $uid = 0;
        $sql = "select u_id, u_rank from " . $this->dbtable->get('t_user', $param['uid']) . " where u_id=? for update;";
    	$query = $this->db->query($sql, array($param['uid']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$rank = $query->row()->u_rank;
    		$uid = $query->row()->u_id;
    		$query->free_result();
    	}
    	
    	if ($uid != $param['uid']) return $return;
    	 
    	$limit = 0;
    	$index = 0;
        $v = $this->cache->get('t_item_plane_index_rank_' . $param['plane']);
        if ($v)
        {
        	$limit = $v['rank'];
        	$index = $v['index'];
        }
        else
        {
            $sql = "select ip_index, ip_rank from " . $this->dbtable->get('t_item_plane') . " where ip_index=? for update;";
    		$query = $this->db->query($sql, array($param['plane']));
    		if (!empty($query) && $query->num_rows() > 0)
    		{
    			$limit = $query->row()->ip_rank;
    			$index = $query->row()->ip_index;
            	$this->cache->save('t_item_plane_index_rank_' . $param['plane'], array('rank' => $limit, 'index' => $index), 3600);
    			$query->free_result();
    		}
        }
    	
    	if ($index != $param['plane'] OR $rank < $limit) return $return;
        
    	$sql = "update " . $this->dbtable->get('t_user_plane', $param['uid']) . " set up_use=0 where up_u_id=?;";
   		if (!$this->db->query($sql, array($param['uid'])))
   		{
   			throw new Exception ('user_plane_m - save - ' . $this->db->last_query());
   		}
    	 
        $sql = "insert into " . $this->dbtable->get('t_user_plane', $param['uid']) . "(up_u_id, up_plane, up_use, up_time) values(?, ?, 1, now()) 
        		on duplicate key update up_use=values(up_use), up_time=values(up_time);";
   		if (!$this->db->query($sql, array($param['uid'], $param['plane'])))
   		{
   			throw new Exception ('user_plane_m - save - ' . $this->db->last_query());
   		}
   		
   		if (!$this->db->trans_complete())
    	{
    		throw new Exception('user_plane_m - save - transaction');
    	}
   		
    	$return['result'] = TRUE;

    	return $return;
    }

    function repair($param = array())
    {
        $return['result'] = FALSE;

        $this->db->trans_start();
    	 
    	$durability = 0;
    	$sql = "select up_durability from " . $this->dbtable->get('t_user_plane', $param['uid']) . " where up_u_id=? and up_plane=? for update;";
    	$query = $this->db->query($sql, array($param['uid'], $param['plane']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$durability = $query->row()->up_durability;
    		$query->free_result();
    	}
    	
    	if ($param['repair'] > $durability) return $return;
    		
        $repair = 0;
        $v = $this->cache->get('t_item_plane_repair_' . $param['plane']);
        if ($v)
        {
        	$repair = $v['repair'] * $param['repair'];
        }
        else
        {
            $sql = "select ip_repair from " . $this->dbtable->get('t_item_plane', $param['uid']) . " where ip_index=? for update;";
    		$query = $this->db->query($sql, array($param['plane']));
    		if (!empty($query) && $query->num_rows() > 0)
    		{
    			$repair = $query->row()->ip_repair * $param['repair'];
            	$this->cache->save('t_item_plane_repair_' . $param['plane'], array('repair' => $query->row()->ip_repair), 3600);
    			$query->free_result();
    		}
        }
    	 
    	$medal = 0;
    	$sql = "select u_medal from " . $this->dbtable->get('t_user', $param['uid']) . " where u_id=? for update;";
    	$query = $this->db->query($sql, array($param['uid']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$medal = $query->row()->u_medal;
    		$query->free_result();
    	}
    	
    	if ($repair > $medal) return $return;
    	
    	$return['medal'] = $repair;
    	
   		$sql = "update " . $this->dbtable->get('t_user_plane', $param['uid']) . " set up_durability=up_durability-? where up_u_id=? and up_plane=?;";
   		if (!$this->db->query($sql, array($param['repair'], $param['uid'], $param['plane'])))
   		{
   			throw new Exception ('user_plane_m - repair - ' . $this->db->last_query());
   		}

   		$sql = "update " . $this->dbtable->get('t_user', $param['uid']) . " set u_medal=u_medal-? where u_id=?;";
   		if (!$this->db->query($sql, array($repair, $param['uid'])))
   		{
   			throw new Exception ('user_plane_m - repair - ' . $this->db->last_query());
   		}
   		 
   		$sql = "insert into " . $this->dbtable->get('t_purchase_repair', $param['uid']) . "(pr_u_id, pr_plane, pr_value, pr_medal, pr_time) values(?, ?, ?, ?, now());";
   		if (!$this->db->query($sql, array($param['uid'], $param['plane'], $param['repair'], $repair)))
   		{
   			throw new Exception ('user_plane_m - repair - ' . $this->db->last_query());
   		}
   		 
   		if (!$this->db->trans_complete())
    	{
    		throw new Exception('user_plane_m - repair - transaction');
    	}
   		
    	$return['result'] = TRUE;

    	return $return;
    }
    
    function spend($param = array())
    {
    	$return['durability'] = 0;
        $sql = "select up_durability from " . $this->dbtable->get('t_user_plane', $param['uid']) . " where up_u_id=? and up_use=1 for update;";
    	$query = $this->db->query($sql, array($param['uid']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return['durability'] = $query->row()->up_durability + 10;
    		$query->free_result();
    	}
    	
    	if ($return['durability'] > 100) $return['durability'] = 100;    	
    	$sql = "update " . $this->dbtable->get('t_user_plane', $param['uid']) . " set up_durability=? where up_u_id=? and up_use=1;";
       	if (!$this->db->query($sql, array($return['durability'], $param['uid'])))
   		{
   			throw new Exception ('user_plane_m - spend - ' . $this->db->last_query());
   		}
   		
   		return $return;
    }
}

/* End of file */