<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Battle_m extends CI_Model
{
    function __construct()
 	{
        parent::__construct();

   		$this->load->database();
   	}
   	
   	function start($param = array())
   	{
        $return['result'] = FALSE;
        
        $this->db->trans_start();
    	 
        $this->load->model('asfb/user_part_m', 'user_partm');
   		$this->user_partm->spend($param);
        
        $this->load->model('asfb/user_plane_m', 'user_planem');
   		$return += $this->user_planem->spend($param);
   		
    	if (!$this->db->trans_complete())
    	{
    		throw new Exception('battle_m - start - transaction');
    	}

        $return['result'] = TRUE;
   		
        return $return;
   	}
   	
    function save($param = array())
    {
        $return['result'] = FALSE;
        
        $this->db->trans_start();
    	 
    	$this->load->model('asfb/user_record_m', 'user_recordm');
    	$return += $this->user_recordm->save_battle($param);
   		
    	$this->load->model('asfb/user_m', 'userm');
    	$return += $this->userm->save_battle($param);
    	
        if (!$this->db->trans_complete())
    	{
    		throw new Exception('battle_m - save - transaction');
    	}

    	$return['result'] = TRUE;
   		
        return $return;
    }
}

/* End of file */