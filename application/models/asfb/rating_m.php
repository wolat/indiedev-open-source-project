<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rating_m extends CI_Model
{
    function __construct()
  	{
        parent::__construct();

  		$this->load->database();
  		
  		$this->load->model('sc/user_m', 'um');
  	}

    function rating_list($fr_uid = 0)
    {
        $return['result'] = FALSE;

    	$pfrids = explode(',', $fr_uid);
    
		$key = 'rating:';
    	$replies = redis()->pipeline(function($pipe) use($key, $pfrids)
    	{
    		foreach ($pfrids as $id) $pipe->zscore($key, $id);
    	});
    		
    	$arr = array();
    	foreach ($replies as $i => $score)
    	{
			$pret['uid'] = $pfrids[$i];
        	$pret['rating_point'] = $score;
        	$pret['last_time'] = $this->um->get_lasttime($pfrids[$i]);
        	$arr[] = $pret;
    	}

    	$return['list'] = $arr;
    	
    	$return['result'] = TRUE;
    	
    	return $return;
	}
	/*
	function sns_rating_list($sns_rating_list = 0)
	{
		$return['result'] = FALSE;
	
		$pfrids = explode(',', $fr_uid);
	
		$key_by_sns = 'sns_rating:';
		$replies = redis()->pipeline(function($pipe) use($key_by_sns, $pfrids)
		{
			foreach ($pfrids as $id) $pipe->zscore($key_by_sns, $id);
		});
	
		$arr = array();
		foreach ($replies as $i => $score)
		{
			$pret['uid'] = $pfrids[$i];
			$pret['rating_point'] = $score;
			$pret['last_time'] = $this->um->get_lasttime($pfrids[$i]);
			$arr[] = $pret;
		}
	
		$return['list'] = $arr;
		 
		$return['result'] = TRUE;
		 
		return $return;
	}
	*/
	
	function sns_rating_list($sns_rating_list = 0)
	{
		$return['result'] = FALSE;
		
		$pfrids = explode(',', $sns_rating_list);
		$pu_ids = array();
		
		for ($i = 0; $i < count($pfrids) ; $i++)
		{
			
			$sql = "select u_id from " . $this->dbtable->get('t_user', $pfrids[$i]) . " where u_sns_id = ? order by u_lasttime desc;";
			$query = $this->db->query($sql, array($pfrids[$i]));
			
			if (!empty($query) && $query->num_rows() > 0)
			{
				$pu_ids[$i] = $query->row()->u_id;
			}
			else 
			{
				$pu_ids[$i] = -1;
			}
		}
		
		$key = 'rating:';
		$replies = redis()->pipeline(function($pipe) use($key, $pu_ids)
		{
			foreach ($pu_ids as $id) $pipe->zscore($key, $id);
		});
		
		$arr = array();
		foreach ($replies as $i => $score)
		{
			$pret['uid'] = $pu_ids[$i];
			$pret['sns_id'] = $pfrids[$i];
			$pret['rating_point'] = $score;
			$pret['last_time'] = $this->um->get_lasttime($pu_ids[$i]);
			$arr[] = $pret;
		}
		
		$return['list'] = $arr;
		 
		$return['result'] = TRUE;
		 
		return $return;
	}
	
	function insert_rating($puserid = 0, $popid = 0, $presult = 0)
	{	
		$redis = redis();
		$key = 'rating:';
//		$weekly_Key = 'rating:' . date('o') . '_' . intval(date('W'));
		
		$rating1 = $redis->zscore($key, $puserid);
		if ($rating1 == null) $rating1 = 1200;
		$rating2 = $redis->zscore($key, $popid);
		if ($rating2 == null) $rating2 = 1200;
		
		$result = ($presult > 1 ? (($presult >= 3) ? 0.5 : 0) : 1);
		$point1 = floor(35 * ($result - 1 / (1 + pow(10, ($rating2 - $rating1) / 400))));
		$point2 = floor(35 * (1 - $result - 1 / (1 + pow(10, ($rating1 - $rating2) / 400))));
		
		$rating1 += $point1;
		$rating2 += $point2;
		
		$rating1 = max(0, $rating1);
		$rating2 = max(0, $rating2);
		
		$redis->zadd($key, $rating1, $puserid);
		$redis->zadd($key, $rating2, $popid);
		
		$return = array($rating1, $point1);
		
//		$redis->zadd($weekly_Key, $rating1, $puserid);
//		$redis->zadd($weekly_Key, $rating2, $popid);
		
		$sql = "insert into " . $this->dbtable->get('t_rating', $puserid) . "(r_u_id, r_point, r_time) values(?, ?, now()), (?, ?, now()) on duplicate key update r_point=values(r_point), r_time=values(r_time);";
        if (!$this->db->query($sql, array($puserid, $rating1, $popid, $rating2)))
        {
	        throw new Exception ('insert_rating - ' . $this->db->last_query());
        }
        
        if ($presult == 1)
        {
        	$enable = 0;
        }
        else
        {
        	$enable = 1;
        }
        
        $sql = "insert into " . $this->dbtable->get('t_battle', $puserid) . "(b_index, b_u_id, b_op_id, b_u_rating, b_op_rating, b_battletime, b_enable) values (0,?,?,?,?,UNIX_TIMESTAMP(NOW()),?);";
        if (!$this->db->query($sql, array($puserid, $popid, $point1, $point2, $enable)))
        {
        	throw new Exception ('insert_rating - ' . $this->db->last_query());
        }
        
        return $return;
	}
	
	function insert_weekly_rating($puserid = 0, $popid = 0, $presult = 0)
	{
		$redis = redis();
		$weekly_key = 'rating:' . date('o') . '_' . intval(date('W'));
		
		$rating1 = $redis->zscore($weekly_key, $puserid);
		if ($rating1 == null) $rating1 = 1200;
		$rating2 = $redis->zscore($weekly_key, $popid);
		if ($rating2 == null) $rating2 = 1200;
	
		$result = ($presult > 1 ? (($presult >= 3) ? 0.5 : 0) : 1);
		$point1 = floor(35 * ($result - 1 / (1 + pow(10, ($rating2 - $rating1) / 400))));
		$point2 = floor(35 * (1 - $result - 1 / (1 + pow(10, ($rating1 - $rating2) / 400))));
	
		$rating1 += $point1;
		$rating2 += $point2;
	
		$rating1 = max(0, $rating1);
		$rating2 = max(0, $rating2);
	
		$redis->zadd($weekly_key, $rating1, $puserid);
		$redis->zadd($weekly_key, $rating2, $popid);
		
		$week = date('o') . '_' . intval(date('W'));
		$sql = "insert into " . $this->dbtable->get('t_rating_' . $week, $puserid) . "(r_u_id, r_point, r_time) values(?, ?, now()), (?, ?, now()) on duplicate key update r_point=values(r_point), r_time=values(r_time);";
		if (!$this->db->query($sql, array($puserid, $rating1, $popid, $rating2)))
		{echo $this->db->last_query();
			throw new Exception ('insert_rating - ' . $this->db->last_query());
		}
	
	}
	
	function get_rating($uid = 0)
	{
		$redis = redis();
		$key = 'rating:';
		
		$rating = $redis->zscore($key, $uid); 
		if ($rating == null) 
		{
			$rating = 1200;

			$redis->zadd($key, $rating, $uid);
			
			$sql = "insert into " . $this->dbtable->get('t_rating', $uid) . "(r_u_id, r_point, r_time) values(?, ?, now()) on duplicate key update r_point=values(r_point), r_time=values(r_time);";
        	if (!$this->db->query($sql, array($uid, $rating)))
        	{
	        	throw new Exception ('get_rating - ' . $this->db->last_query());
        	}
		}
		
		return $rating; 
	}
	
	function get_weekly_rating($uid = 0)
	{
		$redis = redis();
		$weekly_key = 'rating:' . date('o') . '_' . intval(date('W'));
	
		$rating = $redis->zscore($weekly_key, $uid);
		if ($rating == null)
		{
			$rating = 1200;
	
			$redis->zadd($weekly_key, $rating, $uid);
				
			$sql = "insert into " . $this->dbtable->get('t_rating', $uid) . "(r_u_id, r_point, r_time) values(?, ?, now()) on duplicate key update r_point=values(r_point), r_time=values(r_time);";
			if (!$this->db->query($sql, array($uid, $rating)))
			{
				throw new Exception ('get_rating - ' . $this->db->last_query());
			}
		}
	
		return $rating;
	}
	
	function world_ranking($pindex = 0, $pcount = 0)
	{
		$return['result'] = FALSE;
		$pindex--;
		$pcount--;
		
		$result = redis()->zrevrange('rating:', $pindex, $pindex + $pcount, 'withscores');
		$pret = array();
		
		foreach ($result as $i => $info)
		{
			$user_id = $info[0];
			$pret[$i] = array('uid' => $user_id, 'rating_point' => $info[1]);
		}
		
		$return['list'] = $pret;
		 
		$return['result'] = TRUE;
		return $return;
	}
	
	function world_weekly_ranking($pindex = 0, $pcount = 0)
	{
		$return['result'] = FALSE;
		$pindex--;
		$pcount--;
		$weekly_key = 'rating:' . date('o') . '_' . intval(date('W'));
		
		$result = redis()->zrevrange($weekly_key, $pindex, $pindex + $pcount, 'withscores');
		$pret = array();
	
		foreach ($result as $i => $info)
		{
			$user_id = $info[0];
			$pret[$i] = array('uid' => $user_id, 'rating_point' => $info[1]);
		}
	
		$return['list'] = $pret;
			
		$return['result'] = TRUE;
		return $return;
	}
	
	function world_winstreak_ranking($pindex = 0, $pcount = 0)
	{
		$return['result'] = FALSE;
		
		$param = array($pindex, $pcount);
		$sql = "SELECT r_u_id, r_maxwinstreak FROM " . $this->dbtable->get('t_record') . " ORDER BY r_winstreak DESC limit ?, ?;";
		$query = $this->db->query($sql, array(intval($pindex), intval($pcount)));
		if (!empty($query) && $query->num_rows() > 0)
		{
			foreach ($query->result() as $r)
	    	{
	    		$arrtemp = array();
	    		$arrtemp['uid'] = $r->r_u_id;
	    		$arrtemp['max_winstreak'] = $r->r_maxwinstreak;
	    		
	    		$return['list'][] = $arrtemp;
	    	}
	    	
	    	
	    	$return['result'] = TRUE;
		}
		
		return $return;
	}
	
	function world_win_ranking($pindex = 0, $pcount = 0)
	{
		$return['result'] = FALSE;
	
		$param = array($pindex, $pcount);
		$sql = "SELECT r_u_id, r_win FROM " . $this->dbtable->get('t_record') . " ORDER BY r_win DESC limit ?, ?;";
		$query = $this->db->query($sql, array(intval($pindex), intval($pcount)));
		if (!empty($query) && $query->num_rows() > 0)
		{
			foreach ($query->result() as $r)
			{
				$arrtemp = array();
				$arrtemp['uid'] = $r->r_u_id;
				$arrtemp['win'] = $r->r_win;
		   
				$return['list'][] = $arrtemp;
			}
	
	
			$return['result'] = TRUE;
		}
	
		return $return;
	}
	
	
}

/* End of file */