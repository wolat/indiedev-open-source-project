<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Wordfilter_m extends CI_Model 
{
	var $disallowed = array('fuck', 'suck', 'shit', 'ass', 'cunt', 'bitch', 'bastard', 'dick', 'pussy', 'jerk', 'piss', 'cock', 'freak');
		
	var $impersonationi = array('admin', 'moderator');
	
	var $impersonation = array('GM', 'CS');
	
	function __construct()
  	{
        parent::__construct();
  	}

	function word_censor($pword)
	{
		foreach ($this->disallowed as $dis)
    	{
    		$pword = preg_replace('/' . $dis . '/i', '####', $pword);
    	}
    
    	return $pword;
	}

	function find_word($pword)
	{
		foreach ($this->disallowed as $dis)
    	{
    		if (stristr($pword, $dis)) return true;
    	}
    	
    	foreach ($this->impersonationi as $dis)
    	{
    		if (stristr($pword, $dis)) return true;
    	}
    	 
		foreach ($this->impersonation as $dis)
    	{
    		if (strstr($pword, $dis)) return true;
    	}
    	
    	return false;
	}
}

/* End of file */