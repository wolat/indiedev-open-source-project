<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ranking_m extends CI_Model
{
    function __construct()
  	{
        parent::__construct();

  		$this->load->database();
  	}

    function max_score_list($fr_uid = 0)
    {
        $return['result'] = FALSE;

        $ids = $this->dbtable->make_userid_group('t_score', explode(',', $fr_uid));
         
        $pret = array();
        foreach ($ids as $k => $v)
        {
        	$param = array();
        	foreach ($v as $id)
        	{
        		array_push($param, $id);
        	}
        		
        	$sql = "select s_u_id, s_score from " . $k . " where s_u_id in (";
        
        	$count = count($param);
        	$sql .= "?";
        	for ($i = 1; $i < $count; $i++)
        	{
        		$sql .= ", ?";
        	}
        	$sql .= ");";
        
        	foreach ($this->db->query($sql, $param)->result_array() as $member)
        	{
        		$pret[$member['s_u_id']] = $member['s_score'];
        	}
        }
        
        $return['score_list'] = $pret;
        
        $return['result'] = TRUE;
        
        return $return;
    }
    
    function score_list($fr_uid = 0)
    {
        $return['result'] = FALSE;

    	$pfrids = explode(',', $fr_uid);
    
		$key = 'weekly_score:' . date('o') . '_' . intval(date('W'));
    	$replies = redis()->pipeline(function($pipe) use($key, $pfrids)
    	{
    		foreach ($pfrids as $id) $pipe->zscore($key, $id);
    	});
    		
    	$pret = array();
    	foreach ($replies as $i => $score)
    	{
        	$pret[$pfrids[$i]] = $score;
    	}

    	$return['score_list'] = $pret;
    	
    	$return['result'] = TRUE;
    	
    	return $return;
	}
    
	function insert_score($puserid = 0, $pvalue = 0)
	{
		$redis = redis();
		$key = 'weekly_score:' . date('o') . '_' . intval(date('W'));
		
		$rank = $redis->zscore($key, $puserid);
		if ($pvalue > $rank)
		{ 
			$redis->zadd($key, $pvalue, $puserid);
		}

		$this->db->trans_start();

        $score = 0;
		$sql = "select s_score FROM " . $this->dbtable->get('t_score', $puserid) . " where s_u_id = ? for update;";
        $query = $this->db->query($sql, array($puserid));
        if (!empty($query) && $query->num_rows() > 0)
        {
        	$score = $query->row()->s_score;
        	$query->free_result();
        }
        
        if ($pvalue < $score) return;
        
      	$sql = "update " . $this->dbtable->get('t_score', $puserid) . " set s_score = ? where s_u_id = ?;";
        if (!$this->db->query($sql, array($pvalue, $puserid)))
        {
	        throw new Exception ('insert_score - ' . $this->db->last_query());
        }

	    if (!$this->db->trans_complete())
      	{
       		throw new Exception('insert_score - transaction');
       	}
	}
	
	function get_score($uid = 0)
	{
		return redis()->zscore('weekly_score:' . date('o') . '_' . intval(date('W')), $uid); 
	}
	
	function get_user_ranking($uid = 0)
	{
		$pret = array();
		$redis = redis();
		$key = 'rating:';
		$pret['rating_point'] = $redis->zscore($key, $uid);
		if ($pret['rating_point'] === null)
		{
			$pret['rating_point'] = 0;
		}
		$pret['rank'] = $redis->zrevrank($key, $uid);
		if ($pret['rank'] === null)
		{
			$pret['rank'] = 0;
		}
		else
		{
			$pret['rank'] += 1;
		}
		 
		return $pret;
	}
	
	function get_weekly_user_ranking($uid = 0)
	{
		$pret = array();
		$redis = redis();
		$weekly_Key = 'rating:' . date('o') . '_' . intval(date('W'));
		$pret['rating_point'] = $redis->zscore($weekly_Key, $uid);
		if ($pret['rating_point'] === null)
		{
			$pret['rating_point'] = 0;
		}
		$pret['rank'] = $redis->zrevrank($weekly_Key, $uid);
		if ($pret['rank'] === null)
		{
			$pret['rank'] = 0;
		}
		else
		{
			$pret['rank'] += 1;
		}
			
		return $pret;
	}
}

/* End of file */