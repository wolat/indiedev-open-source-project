<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shop_m extends CI_Model
{
    function __construct()
  	{
        parent::__construct();

   		$this->load->database();
  	}

    function buy_part($param = array())
    {
        $return['result'] = FALSE;

        $this->db->trans_start();
    	 
        $pay = 0;
        $part = 0;
        $v = $this->cache->get('t_item_part_pay_part_' . $param['part']);
        if ($v)
        {
        	$pay = $v['pay'];
        	$part = $v['part'];
        }
        else
        {
        	$sql = "select ip_medal, ip_part from " . $this->dbtable->get('t_item_part') . " where ip_index=? for update;";
    		$query = $this->db->query($sql, array($param['part']));
	    	if (!empty($query) && $query->num_rows() > 0)
    		{
    			$pay = $query->row()->ip_medal;
    			$part = $query->row()->ip_part;
            	$this->cache->save('t_item_part_pay_part' . $param['part'], array('pay' => $pay, 'part' => $part), 3600);
    			$query->free_result();
    		}
        }

    	$medal = 0;
    	$sql = "select u_medal from " . $this->dbtable->get('t_user', $param['uid']) . " where u_id=? for update;";
    	$query = $this->db->query($sql, array($param['uid']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$medal = $query->row()->u_medal;
    		$query->free_result();
    	}
    	
    	if ($pay == 0 OR $medal < $pay) return $return;
    		
    	$sql = "insert into " . $this->dbtable->get('t_user_part', $param['uid']) . "(up_u_id, up_item, up_quantity, up_use, up_time, up_part) 
    			values(?, ?, 1, 0, now(), ?) on duplicate key update up_quantity=up_quantity+values(up_quantity), up_time=values(up_time);";
       	if (!$this->db->query($sql, array($param['uid'], $param['part'], $part)))
   		{
   			throw new Exception ('shop_m - buy_part - ' . $this->db->last_query());
   		}
    	
   		$sql = "update " . $this->dbtable->get('t_user', $param['uid']) . " set u_medal=u_medal-? where u_id=?;";
   		if (!$this->db->query($sql, array($pay, $param['uid'])))
   		{
   			throw new Exception ('shop_m - buy_part - ' . $this->db->last_query());
   		}
   		 
        $sql = "insert into " . $this->dbtable->get('t_purchase_part', $param['uid']) . "(pp_u_id, pp_item, pp_quantity, pp_medal, pp_time) values(?, ?, 1, ?, now());";
       	if (!$this->db->query($sql, array($param['uid'], $param['part'], $pay)))
   		{
   			throw new Exception ('shop_m - buy_part - ' . $this->db->last_query());
   		}
   		
   		if (!$this->db->trans_complete())
    	{
    		throw new Exception('shop_m - buy_part - transaction');
    	}
   		
    	$return['medal'] = $medal - $pay;
    	$return['result'] = TRUE;

    	return $return;
    }

    function buy_medal($param = array())
    {
        $return['result'] = FALSE;

        $this->db->trans_start();
    	 
        $pay = 0;
        $medal = 0;
        $v = $this->cache->get('t_item_medal_pay_medal_' . $param['medal']);
        if ($v)
        {
        	$pay = $v['pay'];
        	$medal = $v['medal'];
        }
        else
        {
            $sql = "select im_cash, im_medal from " . $this->dbtable->get('t_item_medal') . " where im_index=? for update;";
    		$query = $this->db->query($sql, array($param['medal']));
    		if (!empty($query) && $query->num_rows() > 0)
    		{	
    			$pay = $query->row()->im_cash;
    			$medal = $query->row()->im_medal;
            	$this->cache->save('t_item_medal_pay_medal_' . $param['medal'], array('pay' => $pay, 'medal' => $medal), 3600);
    			$query->free_result();
    		}
        }

    	$cash = 0;
    	$sql = "select u_cash, u_medal from " . $this->dbtable->get('t_user', $param['uid']) . " where u_id=? for update;";
    	$query = $this->db->query($sql, array($param['uid']));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$cash = $query->row()->u_cash;
    		$um = $query->row()->u_medal;
    		$query->free_result();
    	}
    	
    	if ($pay == 0 OR $cash < $pay) return $return;
    		
   		$sql = "update " . $this->dbtable->get('t_user', $param['uid']) . " set u_cash=u_cash-?, u_medal=u_medal+? where u_id=?;";
   		if (!$this->db->query($sql, array($pay, $medal, $param['uid'])))
   		{
   			throw new Exception ('shop_m - buy_medal - ' . $this->db->last_query());
   		}

   		$sql = "insert into " . $this->dbtable->get('t_purchase_medal', $param['uid']) . "(pm_u_id, pm_item, pm_quantity, pm_cash, pm_time) values(?, ?, ?, ?, now());";
   		if (!$this->db->query($sql, array($param['uid'], $param['medal'], $medal, $pay)))
   		{
   			throw new Exception ('shop_m - buy_medal - ' . $this->db->last_query());
   		}
   		 
   		if (!$this->db->trans_complete())
    	{
    		throw new Exception('shop_m - buy_medal - transaction');
    	}
   		
    	$return['cash'] = $cash - $pay;
    	$return['medal'] = $um + $medal;
    	$return['result'] = TRUE;

    	return $return;
    }
}

/* End of file */