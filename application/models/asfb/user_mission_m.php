<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_mission_m extends CI_Model
{
    function __construct()
  	{
        parent::__construct();

   		$this->load->database();
  	}

    function get($uid = '')
    {
        $return = '';

    	$sql = "select * from " . $this->dbtable->get('t_user_mission', $uid) . " where um_u_id = ?;";
    	$query = $this->db->query($sql, array($uid));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return = $query->result_array();
    		$query->free_result();
    	}
        
        return $return;
    }
}

/* End of file */