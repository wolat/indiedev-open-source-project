<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Item_plane_m extends CI_Model
{
    function __construct()
  	{
        parent::__construct();

   		$this->load->database();
  	}

    function get($uid = '')
    {
        $return = '';

    	$sql = "select up_item, up_quantity, up_use, up_part from " . $this->dbtable->get('t_user_part', $uid) . " where up_u_id = ?;";
    	$query = $this->db->query($sql, array($uid));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return = $query->result_array();
    		$query->free_result();
    	}
        
        return $return;
    }

    function get_use($uid = '')
    {
        $return = '';

    	$sql = "select up_item, up_part from " . $this->dbtable->get('t_user_part', $uid) . " where up_u_id = ? and up_use=1;";
    	$query = $this->db->query($sql, array($uid));
    	if (!empty($query) && $query->num_rows() > 0)
    	{
    		$return = $query->result_array();
    		$query->free_result();
    	}
        
        return $return;
    }
}

/* End of file */