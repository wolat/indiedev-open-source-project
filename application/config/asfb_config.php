<?php  
if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['ASFB']['TITLE'] = 'Full Blast(대격돌)';
$host_url = 'http://52.79.183.20/asfb';
$config['ASFB']['HTTP_JS_LOCATION'] = $host_url . '/js/';
$config['ASFB']['HTTP_CSS_LOCATION'] = $host_url . '/css/';
$config['ASFB']['asfbcms_realm'] = 'asfbcms';
$config['ASFB']['asfbcms_user'] = 'asfbadmin';
$config['ASFB']['asfbcms_pass'] = 'asfbadmin';
$config['ASFB']['asfbcms_ip_allowed'] = array();

$config['ASFB']['SEASON'] = 0;

$config['ASFB']['REVISION'] = '20160425001';

// SET UP OVERRIDES
$override_file = str_replace('.php', '.override.php', __FILE__);
if (file_exists($override_file)) include($override_file);
