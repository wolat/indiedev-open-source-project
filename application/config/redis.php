<?php  
if (!defined('BASEPATH')) exit('No direct script access allowed');

// function for loading redis object.
function redis($name='default') {
    static $list;
    if( ! isset( $list ) ) $list = array();
    if( isset( $list[ $name ] ) ) return $list[$name];
    $servers =  config_item('redis_servers');
    if( ! isset( $servers[$name] ) ) throw new Exception("invalid redis server: $name");
    return $list[ $name ] = new \Predis\Client($servers[$name]['conn'], $servers[$name]['options']);
}

$config = array(
    'redis_servers' => array(
        'default'=>array(
            'conn'=>'tcp://indeidevredis.3tevtd.0001.apn2.cache.amazonaws.com:6379',
            'options'=>array('prefix'=>'asfb:'),
        ),
    ),
);

// SET UP OVERRIDES
$override_file = str_replace('.php', '.override.php', __FILE__);
if (file_exists($override_file)) include ($override_file);
