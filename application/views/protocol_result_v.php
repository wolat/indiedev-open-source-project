<?PHP
if (!defined('BASEPATH')) exit('No direct script access allowed');

echo doctype('xhtml11'); 
?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="utf-8" xml:lang="utf-8">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo $this->dbconfig->fileconfig['TITLE']; ?> Protocol Page</title>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo $this->dbconfig->fileconfig['HTTP_CSS_LOCATION']; ?>jquery.jsonview.css" />
    <style type="text/css">
        * { font-family: "consolas", "dotum", "dotumche"; font-size: 12px; font-weight:bold; }
        body { margin: 0px; padding: 0px; }
        div { position: relative; }
        #wrap { width: 100%; margin: 10px auto; }
    </style>
</head>
<body>
    <div id="wrap">
        <!-- Form -->
        <table style="width: 100%;">
        <tr><th style="text-align: center; height: 30px;"><?=$mode?></th></tr>
        </table>
        <?php if (count($param) > 0): ?>
        <div style="line-height: 20px; height: 20px; border: 2px solid #D1D1D1; border-bottom: none; padding: 5px;">Send Data</div>
        <div style="overflow-y: auto; border: 2px solid #D1D1D1; padding: 5px;">
            <form id="sFrm" name="sFrm">
            <input type="hidden" name="cate" id="cate" value="<?=$mode?>" />
            <table style="width: 100%;">
            <?php foreach ($param as $item): ?>
            <tr>
                <td style="width: 150px;"><?=$item?></td>
                <td><input type="text" name="<?=$item?>" id="<?=$item?>" value="" style="width: 90%;" /></td>
            </tr>
            <?php endforeach; ?>
            </table>
            </form>
        </div>
        <?php endif; ?>
        <!-- Button -->
        <table style="width: 100%; margin-top: 10px;">
        <tr>
            <td style="text-align: center; height: 50px;"><a id="send">Send</a><a id="cancel">Cancel</a></td>
        </tr>
        </table>
        <!-- Result -->
        <div style="line-height: 20px; height: 20px; border: 2px solid #D1D1D1; border-bottom: none; padding: 5px; background-color:#eeeeee;">Result with JSON</div>
        <div style="overflow-y: auto; height: 400px; border: 2px solid #D1D1D1; padding: 5px; word-wrap:break-word; color:#7d2252; background-color:#b7ceec;">
            <xmp id="error_page" name="error_page"></xmp>
        	<div id="result_page" name="result_page"></div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-2.2.3.min.js" integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw=" crossorigin="anonymous"></script>    
    <script src="<?php echo $this->dbconfig->fileconfig['HTTP_JS_LOCATION']; ?>jquery.serializejson.min.js"></script>
    <script src="<?php echo $this->dbconfig->fileconfig['HTTP_JS_LOCATION']; ?>json2.js"></script>
    <script src="<?php echo $this->dbconfig->fileconfig['HTTP_JS_LOCATION']; ?>jquery.jsonview.js"></script>
    <script src="<?php echo $this->dbconfig->fileconfig['HTTP_JS_LOCATION']; ?>jquery.base64.js"></script>
    <script>
    <!--
        $(function(){
            $("#send").button();
            $("#cancel").button();
            $("#cancel").click(function(){
                $("#sFrm")[0].reset();
            });
            $("#send").click(function(){
            	$.base64.utf8encode = true;
            	$.ajax({
					url:"/<?=$ctrpath?>/"+$("#cate").val()+"/",
					contentType:"application/json;charset=utf-8",
					data:JSON.stringify({param:$.base64.btoa(JSON.stringify($("#sFrm").serializeJSON()))}),
					dataType:"text",
                    type:"POST",
                    error:function(request, status, error) {
                        $("#error_page").html("Code : "+request.status+"\nMessage : "+request.responseText+"\nError : "+error);
                    	$("#result_page").html("");
					},
                    success:function(html) {
                    	$("#error_page").html("");
                     	$("#result_page").JSONView($.base64.atob(html,true));
}
                });
            });
        });
    //-->
    </script>
</body>
</html>