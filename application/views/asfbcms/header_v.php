<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<?php echo doctype('xhtml11'); ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="utf-8" lang="utf-8" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $this->dbconfig->fileconfig['TITLE']; ?> CMS</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://code.jquery.com/jquery-2.2.3.min.js" integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw=" crossorigin="anonymous"></script>    
</head>
<body>
<div>