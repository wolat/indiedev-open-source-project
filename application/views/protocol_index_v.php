<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

echo doctype('xhtml11'); 
?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="utf-8" xml:lang="utf-8">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo $this->dbconfig->fileconfig['TITLE']; ?> Protocol Page</title>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <style>
		* { font-size: 12px; font-family: "Consolas"; font-weight:bold; }
		html { height: 100%; }
		body { width: 100%; height: 100%; margin: 0px; padding: 0px; background-color:#ebdde2; }
		iframe { width: 100%; height: 200px; border: none; }
		#wrap { position: relative; margin: 10px auto; width: 1200px; }
		#logo { position: relative; float: left; }
		#gnb { position: relative; float: right; }
		#container { position: relative; float: left; width: 1200px; margin: 0px auto; padding-bottom: 10px; }

		.lnb { position: relative; float: left; width: 200px; background-color:#ccccff; }
		.lnb ul { line-height: 20px; list-style: none; background:transparent; }
		.contents { position: relative; float: right; width: 945px; background-color:#ffe87c; }
		.contents div { background:transparent; }
	</style>
</head>
<body>
    <div id="wrap">
        <div id="container">
            <ul>
            	<?php foreach ($protocol as $item): ?>
            	<li><a href="#tabs-<?=$item?>"><?=$item?> protocol</a></li>
            	<?php endforeach; ?>
            </ul>
            <?php foreach ($protocol as $item): ?>
            <div id="tabs-<?=$item?>">
                <div class="lnb">
					<h3>Protocol</h3>
					<ul>
            			<?php foreach ($sub[$item] as $su): ?>
						<li class="btn-lnb" id="p_<?=$item?>-<?=$su?>"><?=$su?></li>
            			<?php endforeach; ?>
					</ul>
				</div>
				<div class="contents">
					<h3>Form &amp; Result</h3>
					<div id="div_p_<?=$item?>_frame">
						<iframe id="p_<?=$item?>_frame"></iframe>
					</div>
				</div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-2.2.3.min.js" integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw=" crossorigin="anonymous"></script>    
    <script>
    <!--
        $(function(){
            var option_accordion={autoHeight: false, clearStyle: true, navigation: true};
            $("#container").tabs();
            $(".lnb").accordion({ heightStyle: "content" });
			$(".contents").accordion({ heightStyle: "content" });
            $(".btn-lnb").css("cursor", "pointer");
            // Button Event
            $(".btn-lnb").mouseover(function(){
                $(this).css("color", "#FF0000");
            });
            $(".btn-lnb").mouseout(function(){
                $(this).css("color", "#000000");
            });
            $(".btn-lnb").click(function(){
                var tmp=$(this).attr("id").split("-");
                frameResize(tmp[0]+"_frame", "<?=current_url();?>/?mode="+tmp[1]);
            });
        });
        function frameResize(frame, src) {
            $("#"+frame).attr("src", src).load(function(){
                $(this).css("height", "0");
                h=$(this).contents().height();
                $(this).css("height", h);
                $("#div_"+frame).css("height", h+10);
            });
        }
    //-->
    </script>
</body>
</html>