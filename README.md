### 같이 게임을 제작하거나 오픈 소스 프로젝트를 진행할 팀원을 찾고 있습니다.

### 관심있으신 분은 아래 블로그 오셔서 여러 얘기 나누시면 좋겠습니다.

### [네이버 블로그](http://blog.naver.com/wolat)


※ 본 소스 코드는 [GPL 라이센스](http://www.gnu.org/licenses/gpl-3.0.html) 를 따릅니다. [![gplv3-127x51.png](https://bitbucket.org/repo/z4L9EB/images/2216783011-gplv3-127x51.png)](http://www.gnu.org/licenses/gpl-3.0.html)

- [JQuery 라이센스](https://jquery.org/license/)

※ [유니티 데모 (WebGL 버전)](http://52.79.183.20/asfb/demo/unity/index.html)

- 멀티플레이는 포톤 엔진을 사용하며 동접 100명까지입니다.

※ [CMS 데모](http://52.79.183.20/asfb/index.php/asfbcms/user)

- 구축중에 있으며 접근 계정과 비밀번호는 소스 보면 아실 수 있습니다.

※ [CodeIgniter 매뉴얼](http://codeigniter-kr.org/user_guide_2.1.0/toc.html)

개발에 참여하실 분은 각자 아이디로 branch 만드셔서 작업하시면 됩니다.

개발 목적이 아닌 상용으로 사용하신다면 메일(wolat.kim@gmail.com) 로 알려주시거나 출처 정도는 표기해주시기를 바랍니다.

그리고 프로젝트 의뢰는 메일로 주시면 됩니다.

[![icon192.png](https://bitbucket.org/repo/z4L9EB/images/4236102649-icon192.png)](https://play.google.com/store/apps/details?id=com.bn.wolat.as.tap2pang)
[![mcicon192.jpg](https://bitbucket.org/repo/z4L9EB/images/1499286296-mcicon192.jpg)](https://sketch.bluenoon.net)