CREATE DATABASE  IF NOT EXISTS `asfb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `asfb`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: playplus.co.kr    Database: asfb
-- ------------------------------------------------------
-- Server version	5.5.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_device`
--

DROP TABLE IF EXISTS `t_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_device` (
  `d_id` varchar(128) NOT NULL,
  `d_u_id` bigint(20) unsigned NOT NULL,
  `d_name` varchar(128) DEFAULT NULL,
  UNIQUE KEY `d_id_UNIQUE` (`d_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_device`
--

LOCK TABLES `t_device` WRITE;
/*!40000 ALTER TABLE `t_device` DISABLE KEYS */;
INSERT INTO `t_device` VALUES ('16c10b09737bde71aab6d2746a30cdcca3b5ed68',5,'SW50ZWwoUikgQ29yZShUTSkgaTMgQ1BVIDU0MCBAIDMuMDdHSHogKDYwNzIgTUIp'),('3ff99ced499230b7eee480bd918dfe48',4,'c2Ftc3VuZyBHYWxheHkgTmV4dXM='),('7f8b3cff4a56e79cc69520d967fdd567631b90ba',3,'SW50ZWwoUikgQ29yZShUTSkgaTMgQ1BVIDUzMCBAIDIuOTNHSHogKDMzMjAgTUIp'),('9aad02389de8bd81ded9d4acdbce449bd2bf2ba0',6,'SW50ZWwoUikgQ29yZShUTSkgaTMgQ1BVIDU0MCBAIDMuMDdHSHogKDM5NjAgTUIp'),('f3qtgqfa9wurfijfasf23',1,'d2Vi'),('f3qtgqfa9wurfijfasf24',2,'d2Vi');
/*!40000 ALTER TABLE `t_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_item_medal`
--

DROP TABLE IF EXISTS `t_item_medal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_item_medal` (
  `im_index` int(10) unsigned NOT NULL,
  `im_cash` int(10) unsigned DEFAULT '0',
  `im_medal` int(10) unsigned DEFAULT '0',
  `im_bonus` int(10) unsigned DEFAULT '0',
  UNIQUE KEY `im_index_UNIQUE` (`im_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_item_medal`
--

LOCK TABLES `t_item_medal` WRITE;
/*!40000 ALTER TABLE `t_item_medal` DISABLE KEYS */;
INSERT INTO `t_item_medal` VALUES (1,10,1000,0),(2,20,2000,0),(3,50,5000,0),(4,100,10000,0),(5,500,50000,0);
/*!40000 ALTER TABLE `t_item_medal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_item_part`
--

DROP TABLE IF EXISTS `t_item_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_item_part` (
  `ip_index` int(10) unsigned NOT NULL,
  `ip_part` int(10) unsigned DEFAULT NULL,
  `ip_medal` int(10) unsigned DEFAULT NULL,
  `ip_cash` int(10) unsigned DEFAULT NULL,
  `ip_value` float unsigned DEFAULT NULL,
  `ip_name` varchar(45) DEFAULT NULL,
  `ip_rank` int(10) unsigned DEFAULT '0',
  UNIQUE KEY `p_index_UNIQUE` (`ip_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_item_part`
--

LOCK TABLES `t_item_part` WRITE;
/*!40000 ALTER TABLE `t_item_part` DISABLE KEYS */;
INSERT INTO `t_item_part` VALUES (1,1,100,0,1,NULL,0),(2,1,200,0,2,NULL,1),(3,1,300,0,3,NULL,2),(4,1,400,0,4,NULL,3),(5,1,500,0,5,NULL,4),(6,1,600,0,6,NULL,5),(7,1,700,0,7,NULL,6),(8,1,800,0,8,NULL,7),(9,1,900,0,9,NULL,8),(10,1,1000,0,10,NULL,9),(11,1,1100,0,11,NULL,10),(12,1,1200,0,12,NULL,11),(13,1,1300,0,13,NULL,12),(14,1,1400,0,14,NULL,13),(15,2,100,0,1,NULL,0),(16,2,200,0,2,NULL,1),(17,2,300,0,3,NULL,2),(18,2,400,0,4,NULL,3),(19,2,500,0,5,NULL,4),(20,2,600,0,6,NULL,5),(21,2,700,0,7,NULL,6),(22,2,800,0,8,NULL,7),(23,2,900,0,9,NULL,8),(24,2,1000,0,10,NULL,9),(25,2,1100,0,11,NULL,10),(26,2,1200,0,12,NULL,11),(27,2,1300,0,13,NULL,12),(28,3,100,0,11,NULL,0),(29,3,200,0,12,NULL,1),(30,3,300,0,13,NULL,2),(31,3,400,0,14,NULL,3),(32,3,500,0,15,NULL,4),(33,3,600,0,16,NULL,5),(34,3,700,0,17,NULL,6),(35,4,100,0,0.1,NULL,0),(36,4,200,0,0.2,NULL,2),(37,4,300,0,0.3,NULL,3),(38,4,400,0,0.4,NULL,4),(39,4,500,0,0.5,NULL,5),(40,5,100,0,11,NULL,0),(41,5,200,0,12,NULL,1),(42,5,300,0,13,NULL,2),(43,5,400,0,14,NULL,3),(44,5,500,0,15,NULL,4),(45,5,600,0,16,NULL,5),(46,5,700,0,17,NULL,6);
/*!40000 ALTER TABLE `t_item_part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_item_plane`
--

DROP TABLE IF EXISTS `t_item_plane`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_item_plane` (
  `ip_index` int(10) unsigned NOT NULL,
  `ip_name` varchar(45) DEFAULT NULL,
  `ip_cash` int(10) unsigned DEFAULT '0',
  `ip_medal` int(10) unsigned DEFAULT '0',
  `ip_repair` int(10) unsigned DEFAULT '0',
  `ip_rank` int(10) unsigned DEFAULT '0',
  UNIQUE KEY `ip_index_UNIQUE` (`ip_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_item_plane`
--

LOCK TABLES `t_item_plane` WRITE;
/*!40000 ALTER TABLE `t_item_plane` DISABLE KEYS */;
INSERT INTO `t_item_plane` VALUES (1,NULL,0,0,5,0),(2,NULL,0,0,5,0),(3,NULL,0,0,5,0),(4,NULL,0,0,5,0),(5,NULL,0,0,5,0),(6,NULL,0,0,5,0),(7,NULL,0,0,5,0),(8,NULL,0,0,5,0);
/*!40000 ALTER TABLE `t_item_plane` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_purchase_medal`
--

DROP TABLE IF EXISTS `t_purchase_medal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_purchase_medal` (
  `pm_index` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pm_u_id` bigint(20) unsigned DEFAULT NULL,
  `pm_item` int(10) unsigned DEFAULT NULL,
  `pm_quantity` int(10) unsigned DEFAULT NULL,
  `pm_cash` int(10) unsigned DEFAULT NULL,
  `pm_time` datetime DEFAULT NULL,
  UNIQUE KEY `pm_index_UNIQUE` (`pm_index`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_purchase_medal`
--

LOCK TABLES `t_purchase_medal` WRITE;
/*!40000 ALTER TABLE `t_purchase_medal` DISABLE KEYS */;
INSERT INTO `t_purchase_medal` VALUES (1,3,1,1000,10,'2014-09-30 12:06:46'),(2,3,1,1000,10,'2014-10-10 14:42:47'),(3,3,1,1000,10,'2014-10-10 14:49:04');
/*!40000 ALTER TABLE `t_purchase_medal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_purchase_part`
--

DROP TABLE IF EXISTS `t_purchase_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_purchase_part` (
  `pp_index` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pp_u_id` bigint(20) unsigned DEFAULT NULL,
  `pp_item` int(10) unsigned DEFAULT NULL,
  `pp_quantity` int(10) unsigned DEFAULT NULL,
  `pp_medal` int(10) unsigned DEFAULT NULL,
  `pp_time` datetime DEFAULT NULL,
  UNIQUE KEY `pp_index_UNIQUE` (`pp_index`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_purchase_part`
--

LOCK TABLES `t_purchase_part` WRITE;
/*!40000 ALTER TABLE `t_purchase_part` DISABLE KEYS */;
INSERT INTO `t_purchase_part` VALUES (1,3,1,1,100,'2014-09-30 12:06:10'),(2,3,1,1,100,'2014-10-01 12:20:00'),(3,3,15,1,100,'2014-10-01 12:21:54'),(4,3,28,1,100,'2014-10-01 12:21:56'),(5,3,35,1,100,'2014-10-01 12:21:58'),(6,3,40,1,100,'2014-10-01 12:21:59'),(7,3,42,1,300,'2014-10-02 12:55:25'),(8,3,29,1,200,'2014-10-02 12:55:28'),(9,3,1,1,100,'2014-10-02 12:55:36'),(10,3,40,1,100,'2014-10-02 12:55:39'),(11,3,1,1,100,'2014-10-10 14:42:22'),(12,3,2,1,200,'2014-10-10 14:49:09');
/*!40000 ALTER TABLE `t_purchase_part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_purchase_plane`
--

DROP TABLE IF EXISTS `t_purchase_plane`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_purchase_plane` (
  `pp_index` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pp_u_id` bigint(20) unsigned DEFAULT NULL,
  `pp_item` int(10) unsigned DEFAULT NULL,
  `pp_cash` int(10) unsigned DEFAULT NULL,
  `pp_time` datetime DEFAULT NULL,
  UNIQUE KEY `pp_index_UNIQUE` (`pp_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_purchase_plane`
--

LOCK TABLES `t_purchase_plane` WRITE;
/*!40000 ALTER TABLE `t_purchase_plane` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_purchase_plane` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_purchase_repair`
--

DROP TABLE IF EXISTS `t_purchase_repair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_purchase_repair` (
  `pr_index` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pr_u_id` bigint(20) unsigned DEFAULT NULL,
  `pr_plane` int(10) unsigned DEFAULT NULL,
  `pr_value` int(10) unsigned DEFAULT NULL,
  `pr_medal` int(10) unsigned DEFAULT NULL,
  `pr_time` datetime DEFAULT NULL,
  UNIQUE KEY `pr_index_UNIQUE` (`pr_index`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_purchase_repair`
--

LOCK TABLES `t_purchase_repair` WRITE;
/*!40000 ALTER TABLE `t_purchase_repair` DISABLE KEYS */;
INSERT INTO `t_purchase_repair` VALUES (1,3,3,10,50,'2014-09-30 12:07:33'),(2,3,3,1,5,'2014-09-30 15:34:25'),(3,3,3,3,15,'2014-09-30 18:20:24'),(4,3,1,10,50,'2014-10-10 14:49:27');
/*!40000 ALTER TABLE `t_purchase_repair` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user`
--

DROP TABLE IF EXISTS `t_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user` (
  `u_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `u_name` varchar(45) DEFAULT NULL,
  `u_cash` int(10) unsigned DEFAULT '0',
  `u_medal` int(10) unsigned DEFAULT '0',
  `u_exp` bigint(20) unsigned DEFAULT '0',
  `u_rank` int(10) unsigned DEFAULT '0',
  `u_regdate` datetime DEFAULT NULL,
  `u_lastdate` datetime DEFAULT NULL,
  UNIQUE KEY `u_id_UNIQUE` (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user`
--

LOCK TABLES `t_user` WRITE;
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` VALUES (1,'7ZWY7ZWY',0,400,400,0,'2014-09-19 15:41:02','2014-09-24 18:26:32'),(2,NULL,0,0,0,0,'2014-09-19 15:42:57','2014-09-19 15:42:57'),(3,'NjY2',9970,2278,867,1,'2014-09-19 16:12:43','2014-10-10 16:59:09'),(4,'66eb7J6I64qU7YyM7KCE',0,1140,1140,1,'2014-09-25 14:51:39','2014-10-08 15:07:10'),(5,'7KiU7Zek7J20',0,680,80,0,'2014-09-25 17:28:32','2014-09-25 17:42:47'),(6,'66eb7JeG64qU7YyM7KCE',0,0,0,0,'2014-09-29 08:27:07','2014-10-09 14:49:10');
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_achievement`
--

DROP TABLE IF EXISTS `t_user_achievement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_achievement` (
  `ua_u_id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_achievement`
--

LOCK TABLES `t_user_achievement` WRITE;
/*!40000 ALTER TABLE `t_user_achievement` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_user_achievement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_mission`
--

DROP TABLE IF EXISTS `t_user_mission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_mission` (
  `um_u_id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_mission`
--

LOCK TABLES `t_user_mission` WRITE;
/*!40000 ALTER TABLE `t_user_mission` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_user_mission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_part`
--

DROP TABLE IF EXISTS `t_user_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_part` (
  `up_u_id` bigint(20) unsigned NOT NULL,
  `up_item` int(10) unsigned DEFAULT NULL,
  `up_quantity` int(10) unsigned DEFAULT '0',
  `up_use` int(10) unsigned DEFAULT '0',
  `up_time` datetime DEFAULT NULL,
  `up_part` int(10) unsigned DEFAULT NULL,
  UNIQUE KEY `index1` (`up_u_id`,`up_item`),
  KEY `index2` (`up_u_id`,`up_use`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_part`
--

LOCK TABLES `t_user_part` WRITE;
/*!40000 ALTER TABLE `t_user_part` DISABLE KEYS */;
INSERT INTO `t_user_part` VALUES (1,1,1,0,'2014-09-24 13:11:20',1),(1,28,1,0,'2014-09-24 13:11:20',3),(1,40,1,0,'2014-09-24 13:11:20',5),(5,14,1,0,'2014-09-25 17:35:24',1),(3,42,1,0,'2014-10-02 12:55:25',5),(3,1,1,0,'2014-10-10 14:42:22',1),(3,2,1,0,'2014-10-10 14:49:09',1);
/*!40000 ALTER TABLE `t_user_part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_passive`
--

DROP TABLE IF EXISTS `t_user_passive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_passive` (
  `up_u_id` bigint(20) unsigned NOT NULL,
  `up_p1` int(10) unsigned DEFAULT '0',
  `up_p2` int(10) unsigned DEFAULT '0',
  `up_p3` int(10) unsigned DEFAULT '0',
  `up_p4` int(10) unsigned DEFAULT '0',
  UNIQUE KEY `up_u_id_UNIQUE` (`up_u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_passive`
--

LOCK TABLES `t_user_passive` WRITE;
/*!40000 ALTER TABLE `t_user_passive` DISABLE KEYS */;
INSERT INTO `t_user_passive` VALUES (1,0,0,0,0),(3,5,0,0,0),(4,6,0,5,0),(5,0,0,0,0),(6,10,0,0,0);
/*!40000 ALTER TABLE `t_user_passive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_plane`
--

DROP TABLE IF EXISTS `t_user_plane`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_plane` (
  `up_u_id` bigint(20) unsigned NOT NULL,
  `up_plane` int(10) unsigned DEFAULT NULL,
  `up_use` int(10) unsigned DEFAULT '0',
  `up_durability` int(10) unsigned DEFAULT '0',
  `up_time` datetime DEFAULT NULL,
  UNIQUE KEY `index1` (`up_u_id`,`up_plane`),
  KEY `index2` (`up_u_id`,`up_use`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_plane`
--

LOCK TABLES `t_user_plane` WRITE;
/*!40000 ALTER TABLE `t_user_plane` DISABLE KEYS */;
INSERT INTO `t_user_plane` VALUES (3,1,0,90,'2014-10-02 16:41:01'),(3,3,0,100,'2014-10-02 17:03:59'),(3,2,0,100,'2014-10-01 15:02:34'),(3,8,0,60,'2014-10-07 17:34:51'),(3,7,0,0,'2014-10-02 12:04:04'),(3,5,1,10,'2014-10-10 14:49:20'),(3,4,0,0,'2014-10-02 17:00:24'),(3,6,0,0,'2014-09-30 12:17:31'),(4,1,1,20,'2014-10-02 17:44:30'),(4,6,0,100,'2014-09-30 18:43:10'),(6,1,1,0,'2014-10-09 14:30:29');
/*!40000 ALTER TABLE `t_user_plane` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_record_0`
--

DROP TABLE IF EXISTS `t_user_record_0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_record_0` (
  `ur_u_id` bigint(20) unsigned NOT NULL,
  `ur_score` bigint(20) unsigned DEFAULT '0',
  `ur_win` int(10) unsigned DEFAULT '0',
  `ur_lose` int(10) unsigned DEFAULT '0',
  `ur_draw` int(10) unsigned DEFAULT '0',
  `ur_dis` int(10) unsigned DEFAULT '0',
  `ur_time` datetime DEFAULT NULL,
  UNIQUE KEY `r_u_id_UNIQUE` (`ur_u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_record_0`
--

LOCK TABLES `t_user_record_0` WRITE;
/*!40000 ALTER TABLE `t_user_record_0` DISABLE KEYS */;
INSERT INTO `t_user_record_0` VALUES (1,0,2,3,0,0,'2014-09-25 16:51:39'),(3,477,21,23,2,0,'2014-10-10 16:59:28'),(4,185,10,15,0,0,'2014-10-08 15:08:40'),(5,0,1,0,0,0,'2014-09-25 17:38:02');
/*!40000 ALTER TABLE `t_user_record_0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_skin`
--

DROP TABLE IF EXISTS `t_user_skin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_skin` (
  `us_u_id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_skin`
--

LOCK TABLES `t_user_skin` WRITE;
/*!40000 ALTER TABLE `t_user_skin` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_user_skin` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-10 17:33:11
