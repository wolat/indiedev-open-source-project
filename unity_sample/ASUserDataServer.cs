﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

public class ASUserDataServer : MonoBehaviour {

	public string URL = "http://test/index.php/asfb/";
	public Info m_Info;
	public HourGlass m_HourGlass;

	class PostDataClass
	{
		public string param;
	}
	PostDataClass m_PostData;

	Encoding m_Encoding = new System.Text.UTF8Encoding();
	Dictionary<string, string> m_Header = new Dictionary<string, string>();

	void Start () {
		m_PostDataClass = new PostDataClass();
		m_Header.Add("Content-Type", "text/json");
	}

	class ParamDataClassGetStart
	{
		public string device;
		public string name;
	}
	class ResponseDataClassGetStart
	{
		public bool result;
		public string revision;
		public string uid;
		public string user;
		public string u_name;
		public int u_cash;
		public int u_medal;
		public int u_rank;
		public int u_exp;
		public List<object> achievement;
		public List<object> mission;
		public List<object> part;
		public Dictionary<string, object> passive;
		public List<object> plane;
		public Dictionary<string, object> record;
		public List<object> skin;
	}
	IEnumerator getStart(string device, string model)
	{
		m_HourGlass.show();

		ParamDataClassGetStart paramData = new ParamDataClassGetStart();
		paramData.device = device;
		paramData.name = model;
		m_PostData.param = Convert.ToBase64String(m_Encoding.GetBytes(JsonUtility.ToJson(paramData)));
		WWW w = new WWW(URL + "start", m_Encoding.GetBytes(JsonUtility.ToJson(m_PostData)), m_Header);
		yield return w;
		if (string.IsNullOrEmpty(w.error))
		{
			ResponseDataClassGetStart d = JsonUtility.FromJson<ResponseDataClassGetStart>(Encoding.UTF8.GetString (Convert.FromBase64String (w.text)));
			if (d.result)
			{
				if (!string.IsNullOrEmpty(d.u_name)) m_Info.setName(Encoding.UTF8.GetString (Convert.FromBase64String (d.u_name)));
			}
		}
		m_HourGlass.close ();
		yield return null;
	}
}
